def partitionize(a: list[int], left: int, right: int) -> int:
    pivot = a[right] # fixed

    i = 0
    for j in range(right):
        if a[j] < pivot:
            if a[i] > a[j]:
                a[i], a[j] = a[j], a[i]
            i += 1

    a[i], a[right] = a[right], a[i]

    return i

def _quicksort(a: list[int], left: int, right: len) -> None:

    if left >= right:
        return

    p = partitionize(a, left, right)
    _quicksort(a, left, p - 1)
    _quicksort(a, p + 1, right)    
    return

def quicksort(a: list[int]):
    _quicksort(a, 0, len(a) - 1)

a = [3, 5, 1, 9, 2, 4, 10, 6, 7]
quicksort(a)
print(a)