
parent = lambda i: (i // 2) if (i % 2) else (i // 2 - 1)
left   = lambda i: (i * 2) + 1
right  = lambda i: left(i) + 1

def bubble_up(a, i):
    if i == 0:
        return

    pi = parent(i)
    if a[pi] > a[i]:
        a[pi], a[i] = a[i], a[pi]
        bubble_up(a, pi)

def bubble_down(a, n, i):
    l = left(i)
    r = right(i)

    #print(f'n = {n} i = {i} l = {l} r = {r}')

    if l > n:
        return

    if r <= n:
        t = l if a[l] < a[r] else r
    else:
        t = l

    #print(f'n = {n} i = {i} l = {l} r = {r}')
    #print(a)

    a[t], a[i] = a[i], a[t]

    #print(a)

    bubble_down(a, n, t)

def heap_insert(a, v):
    a.append(v)
    n = len(a) - 1
    bubble_up(a, n)

def heap_delete(a, i):
    n = len(a) - 1

    if i > len(a) - 1:
        return

    a[i], a[n] = a[n], a[i]
    a.pop(n)

    bubble_up(a, i)
    bubble_down(a, len(a) - 1, i)

def heapify(a):
    n = len(a) - 1
    for i in reversed(range(len(a))):
        bubble_down(a, n, i)

def _heapsort(a, n):
    if n > len(a) - 1:
        return

    a[0], a[n] = a[n], a[0]
    bubble_down(a, n - 1, 0)

def heapsort(a):
    heapify(a)
    for i in reversed(range(len(a))):
        _heapsort(a, i)
    for i in range(len(a) // 2):
        j = len(a) - 1 - i
        a[i], a[j] = a[j], a[i]

a = [2, 4, 6, 6, 5, 7, 8, 9, 10]
#heap_insert(a, 1)
#print(a)
#heap_delete(a, 0)
#print(a)

b = [10, 9, 8, 7, 5, 6, 6, 4, 2]
#heapify(b)
#print(b)

c = [10, 9, 8, 7, 5, 6, 6, 4, 2]
heapsort(c)
print(c)