#!/usr/bin/env python3

import sys

def insert(a):
    loop = 0
    swap = 0
    for i in range(len(a)-1):
        for j in reversed(range(i+1)):
            loop += 1
            if a[j] > a[j+1]:
                a[j], a[j+1] = a[j+1], a[j]
                swap += 1
            else:
                break

def main():
    a = [int(x) for x in sys.argv[1:]]
    insert(a)
    print(a)

if __name__ == '__main__':
    main()