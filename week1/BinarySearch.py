#!/usr/bin/env python3

import sys

def BinarySearch(lst, elt, left, right):
    if left > right:
        print('None')
        return

    mid = (left + right) // 2
    if lst[mid] == elt:
        print(elt)
        return
    
    if lst[mid] < elt:
        return BinarySearch(lst, elt, mid+1, right)
    else:
        return BinarySearch(lst, elt, left, mid-1)

def main() -> None:
    lst = [int(x) for x in sys.argv[1::]]
    n = len(lst)
    elt = lst[n - 1]
    lst.pop(n - 1)

    if elt < lst[0] or elt > lst[n - 1]:
        print('None')
        return

    BinarySearch(lst, elt, 0, len(lst)-1)

if __name__ == '__main__':
    main()