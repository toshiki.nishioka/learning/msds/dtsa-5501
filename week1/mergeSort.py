#!/usr/bin/env python3

import sys

def merge(a, left, mid, right):
    i = left
    j = mid + 1
    output = []

    while i <= mid or j <= right:
        if i <= mid and j <= right:
            if a[i] <= a[j]:
                output.append(a[i])
                i += 1
            else:
                output.append(a[j])
                j += 1
        elif i <= mid:
            output.append(a[i])
            i += 1
        else:
            output.append(a[j])
            j += 1

    for i in range(left, right + 1):
        a[i] = output[i - left]

def mergeSort(a, left, right):
    if len(a) <= 0:
        return
    
    if left >= right:
        return
    
    if left + 1 == right: # adjecent i.e. array of two elements
        if a[left] > a[right]:
            a[left], a[right] = a[right], a[left]
        return
    
    mid = (left + right) // 2
    mergeSort(a, left, mid)
    mergeSort(a, mid + 1, right)
    merge(a, left, mid, right)


def main():
    a = [int(x) for x in sys.argv[1::]]
    mergeSort(a, 0, len(a) - 1)
    print(a)

if __name__ == '__main__':
    main()